//
//  SOSDeviceTableViewCell.h
//  DeviceTracking
//
//  Created by Punit on 21/09/18.
//  Copyright © 2018 Eshiksa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SOSDeviceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nickname;
@property (weak, nonatomic) IBOutlet UILabel *deviceNumber;
@property (weak, nonatomic) IBOutlet UIButton *switchBtn;
@property (weak, nonatomic) IBOutlet UIImageView *watchImg;
@property (weak, nonatomic) IBOutlet UILabel *deviceId;
@end

NS_ASSUME_NONNULL_END
