//
//  SOSViewController.m
//  DeviceTracking
//
//  Created by Punit on 21/09/18.
//  Copyright © 2018 Eshiksa. All rights reserved.
//

#import "SOSViewController.h"
#import "SOSDeviceTableViewCell.h"
#import "Base.h"
#import "SOSListViewController.h"
@interface SOSViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,retain)NSString *status,*message,*stage,*indxp,*deviceNumber,*nickname,*deviceId;

@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property (nonatomic,strong) NSMutableArray *deviceListArr;

@end

@implementation SOSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _deviceListArr=[[NSMutableArray alloc]init];
    
    _tableview.delegate=self;
    _tableview.dataSource=self;
    
    [self.tableview setSeparatorColor:[UIColor clearColor]];
    [self parsingDeviceList];
   
}
-(void)parsingDeviceList{
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
  //  NSLog(@"***username ==%@",username);
    
    NSDictionary *data = @{ @"username":username
                            };
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:kNilOptions error:&error];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:getDeviceListByuserName]]];
    
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url cachePolicy:nil timeoutInterval:60];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
    [req setHTTPMethod:@"POST"];
    [req setHTTPBody:jsonData];
    
    NSString *retStr = [[NSString alloc] initWithData:[NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil] encoding:NSUTF8StringEncoding];
    
    //NSLog(@"Return resistration String%@",retStr);
    
    NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:[NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil] options:NSJSONReadingMutableContainers error:nil];
   // NSLog(@"response data:%@",maindic);
    
    self.status=[maindic objectForKey:@"status"];
    self.message=[maindic objectForKey:@"message"];
    
    NSArray *detailArr=[maindic objectForKey:@"deviceList"];
    
    if(detailArr.count==0)
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"Currently there is no device list." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertView dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        
        [alertView addAction:ok];
        
        [self presentViewController:alertView animated:YES completion:nil];
        
    }
    else {
        
        for(NSDictionary *temp in detailArr)
        {
            NSString *str1=[[temp objectForKey:@"id"]description];
            NSString *str2=[[temp objectForKey:@"deviceNumber"]description];
            NSString *str3=[[temp objectForKey:@"nickName"]description];
            NSString *str4=[[temp objectForKey:@"mobileNumber"]description];
            NSString *str5=[[temp objectForKey:@"username"]description];
            
            [self->_deviceListArr addObject:temp];
            //NSLog(@"gallery ListArr ARRAYY%@",self->_deviceListArr);
        }
    }
    [self.tableview performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableview reloadData];
    });
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _deviceListArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOSDeviceTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSMutableDictionary *ktemp=[_deviceListArr objectAtIndex:indexPath.row];
    
    cell.deviceNumber.text=[[ktemp objectForKey:@"deviceNumber"]description];
    cell.nickname.text=[[ktemp objectForKey:@"nickName"]description];
    cell.deviceId.text=[[ktemp objectForKey:@"id"]description];
    
    
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 91;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SOSDeviceTableViewCell *cell=[_tableview cellForRowAtIndexPath:indexPath];
    _deviceNumber=cell.deviceNumber.text;
    _nickname=cell.nickname.text;
    _deviceId=cell.deviceId.text;
    
    _indxp=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
    
   // NSLog(@"indexpath==%ld & nickname==%@",(long)indexPath.row,_nickname);
    
    [self performSegueWithIdentifier:@"sosDetails"
                              sender:[self.tableview cellForRowAtIndexPath:indexPath]];
//    [[NSUserDefaults standardUserDefaults] setObject:cell.nickname.text forKey:@"nickname"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//
//    [[NSUserDefaults standardUserDefaults] setObject:cell.deviceNumber.text forKey:@"devicenumber"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"sosDetails"])
    {
        
        SOSListViewController *kvc = [segue destinationViewController];
        
        kvc.deviceNum=_deviceNumber;
        kvc.nickname=_nickname;
        kvc.indxp=_indxp;
        
       // NSLog(@"indexpath in prepare for segue==%@",_deviceNumber);
    }
}


@end
